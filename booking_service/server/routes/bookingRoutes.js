const express = require("express");
const {
  addCarToBooking,
  getBookingById,
  getAllBookingsByUser,
} = require("../controllers/bookingController");
const authenticateJWT = require("../middleware/auth");

const router = express.Router();

router.post("/api/bookings/add", authenticateJWT, addCarToBooking);
router.get("/api/bookings/:bookingId", authenticateJWT, getBookingById);
router.get("/api/bookings", authenticateJWT, getAllBookingsByUser);

module.exports = router;
