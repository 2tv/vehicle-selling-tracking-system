const axios = require("axios");
const Booking = require("../models/bookingModel");
const rateLimit = require("express-rate-limit");
const { default: mongoose } = require("mongoose");

// Middleware rate limiter
const limiter = rateLimit({
  windowMs: 60 * 1000, // 1 phút
  max: 5, // số lượng yêu cầu tối đa trong 1 phút
  message: "Bạn đã vượt quá số lần yêu cầu cho phép trong 1 phút",
});

// Middleware xử lý sau khi vượt quá ngưỡng
const handleRateLimitExceeded = (req, res, next) => {
  if (req.rateLimit && req.rateLimit.remaining === 0) {
    console.log("Người dùng đã vượt quá số lần yêu cầu cho phép trong 1 phút");
  }
  next();
};

// Controller cho thêm xe vào booking
exports.addCarToBooking = [
  limiter,
  handleRateLimitExceeded,
  async (req, res) => {
    const { carId } = req.body;
    const userId = req.user.id;

    try {
      const carResponse = await axios.get(
        `${process.env.CAR_SERVICE_URL}/api/cars/${carId}`,
        {
          headers: {
            Authorization: req.header("Authorization"),
          },
        }
      );
      const carDetails = carResponse.data;

      const newBooking = new Booking({ userId, carId, carDetails });
      await newBooking.save();

      res.status(201).json({
        message: "Car added to booking successfully",
        booking: newBooking,
      });
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .json({ message: "An error occurred", error: error.message });
    }
  },
];

// Controller cho lấy booking theo ID
exports.getBookingById = [
  limiter,
  handleRateLimitExceeded,
  async (req, res) => {
    const bookingId = req.params.bookingId;
    const bookingIdObject = new mongoose.Types.ObjectId(bookingId)
    try {
      const booking = await Booking.findById(bookingIdObject);
      if (!booking) {
        return res.status(404).json({ message: "Booking not found" });
      }
      res.status(200).json(booking);
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .json({ message: "An error occurred", error: error.message });
    }
  },
];

// Controller cho lấy tất cả bookings của user
exports.getAllBookingsByUser = [
  limiter,
  handleRateLimitExceeded,
  async (req, res) => {
    const userId = req.user.id;

    try {
      const bookings = await Booking.find({ userId });
      if (bookings.length === 0) {
        return res
          .status(404)
          .json({ message: "No bookings found for this user" });
      }
      res.status(200).json(bookings);
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .json({ message: "An error occurred", error: error.message });
    }
  },
];
