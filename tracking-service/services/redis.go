package services

import (
	"context"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
)

var RedisClient *redis.Client

// ConnectRedis connects to Redis
func ConnectRedis() {
	RedisClient = redis.NewClient(&redis.Options{
		// Addr: "localhost:6379", // Redis address
		Addr: "redis:6379", // Redis address
		DB:   0,            // use default DB
	})

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := RedisClient.Ping(ctx).Result()
	if err != nil {
		log.Fatalf("Failed to connect to Redis: %v", err)
	}

	log.Println("Connected to Redis")
}
