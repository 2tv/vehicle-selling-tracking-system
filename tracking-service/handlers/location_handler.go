package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
	"tracking-service/models"
	"tracking-service/services"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// CHeck JWT Token
func GetAuthorizationHeader(r *http.Request) string {
	authHeader := r.Header.Get("Authorization")
	return authHeader
}

// GetCarLocation handles the request to get the car location
func GetCarLocation(res http.ResponseWriter, req *http.Request) {
	// Check header "Authorization"
	authToken := GetAuthorizationHeader(req)
	if authToken == "" {
		http.Error(res, "Not Login", http.StatusUnauthorized)
		return
	}
	// Get the collection from MongoDB
	collection := services.GetCollection("trackingDB", "locations")

	// Extract the car_id query parameter
	carID := req.URL.Query().Get("car_id")
	if carID == "" {
		http.Error(res, "car_id parameter is required", http.StatusBadRequest)
		return
	}

	// Define a variable to hold the result
	var location models.CarLocation

	// Create a context with a timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Define a filter
	filter := bson.M{"car_id": carID}

	// Define options to sort by timestamp descending and limit to 1
	findOptions := options.FindOne().SetSort(bson.D{{Key: "timestamp", Value: -1}})

	// Find one document from the collection
	err := collection.FindOne(ctx, filter, findOptions).Decode(&location)
	if err != nil {
		// If an error occurs, return a 404 status code with an error message
		http.Error(res, "Location not found", http.StatusNotFound)
		return
	}

	res.Header().Set("Content-Type", "application/json")
	json.NewEncoder(res).Encode(location)
}

// GetCarLocation handles the request to get the car location history
func GetCarLocationHistory(w http.ResponseWriter, r *http.Request) {
	// Check header "Authorization"
	authToken := GetAuthorizationHeader(r)
	if authToken == "" {
		http.Error(w, "Not Login", http.StatusUnauthorized)
		return
	}
	// Extract the car_id query parameter
	carID := r.URL.Query().Get("car_id")
	if carID == "" {
		http.Error(w, "car_id parameter is required", http.StatusBadRequest)
		return
	}

	// Create a context with a timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Try to get the location history from Redis
	locationHistoryJSON, err := services.RedisClient.LRange(ctx, "car_location:"+carID, 0, -1).Result()
	if err != nil {
		http.Error(w, "Failed to get location history from Redis", http.StatusInternalServerError)
		return
	}

	// Unmarshal the JSON result from Redis
	var locations []models.CarLocation
	for _, locJSON := range locationHistoryJSON {
		var location models.CarLocation
		err = json.Unmarshal([]byte(locJSON), &location)
		if err != nil {
			http.Error(w, "Failed to unmarshal location from Redis", http.StatusInternalServerError)
			return
		}
		locations = append(locations, location)
	}

	// Get car information from the API
	carInfo, err := GetCarInfo(carID, authToken)
	if err != nil {
		http.Error(w, "Failed to get car information from API", http.StatusInternalServerError)
		return
	}
	// Create a response struct combining car information and location history
	response := struct {
		CarInfo         models.CarInfo       `json:"car_info"`
		LocationHistory []models.CarLocation `json:"location_history"`
	}{
		CarInfo:         carInfo,
		LocationHistory: locations,
	}

	// Set the content type to application/json
	w.Header().Set("Content-Type", "application/json")

	// Encode the result as JSON and write it to the response
	json.NewEncoder(w).Encode(response)
}

// GetCarInfo gửi yêu cầu HTTP để lấy thông tin của xe từ API
func GetCarInfo(carID string, authToken string) (models.CarInfo, error) {
	// Tạo yêu cầu HTTP
	// req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:8888/api/cars/%s", carID), nil)
	req, err := http.NewRequest("GET", fmt.Sprintf("http://api-gateway:8888/api/cars/%s", carID), nil)

	if err != nil {
		fmt.Println("Error creating HTTP request:", err)
		return models.CarInfo{}, err
	}

	// Thiết lập header authToken
	req.Header.Set("Authorization", authToken)

	// Gửi yêu cầu HTTP và nhận phản hồi
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("Error sending HTTP request:", err)
		return models.CarInfo{}, err
	}
	defer resp.Body.Close()

	// Kiểm tra mã trạng thái của phản hồi
	if resp.StatusCode != http.StatusOK {
		fmt.Printf("Failed to get car information, status code: %d\n", resp.StatusCode)
		return models.CarInfo{}, fmt.Errorf("failed to get car information, status code: %d", resp.StatusCode)
	}

	// Giải mã JSON phản hồi vào biến carInfo
	var carInfo models.CarInfo
	err = json.NewDecoder(resp.Body).Decode(&carInfo)
	if err != nil {
		fmt.Println("Error decoding JSON response:", err)
		return models.CarInfo{}, err
	}

	// Trả về thông tin xe
	return carInfo, nil
}

// CreateLocation handles the request to create a new car location
func CreateLocation(w http.ResponseWriter, r *http.Request) {
	// Check header "Authorization"
	authToken := GetAuthorizationHeader(r)
	if authToken == "" {
		http.Error(w, "Not Login", http.StatusUnauthorized)
		return
	}
	// Decode the JSON payload
	var location models.CarLocation
	err := json.NewDecoder(r.Body).Decode(&location)
	if err != nil {
		http.Error(w, "Invalid JSON payload", http.StatusBadRequest)
		return
	}

	// Set the current timestamp
	location.Timestamp = time.Now()

	// Get the collection from MongoDB
	collection := services.GetCollection("trackingDB", "locations")

	// Create a context with a timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Insert the location document into the collection
	_, err = collection.InsertOne(ctx, location)
	if err != nil {
		http.Error(w, "Failed to insert location", http.StatusInternalServerError)
		return
	}

	// Convert location to JSON
	locationJSON, err := json.Marshal(location)
	if err != nil {
		http.Error(w, "Failed to marshal location to JSON", http.StatusInternalServerError)
		return
	}

	// Save the location to Redis
	err = services.RedisClient.RPush(ctx, "car_location:"+location.CarID, locationJSON).Err()
	if err != nil {
		http.Error(w, "Failed to save location to Redis", http.StatusInternalServerError)
		return
	}

	// Set the TTL for the key to 6 months
	sixMonths := 6 * 30 * 24 * time.Hour // Số ngày * số giờ trong một ngày
	err = services.RedisClient.Expire(ctx, "car_location:"+location.CarID, sixMonths).Err()
	if err != nil {
		http.Error(w, "Failed to set TTL for location key in Redis", http.StatusInternalServerError)
		return
	}

	// Return a success response
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Location added successfully"))
}

// DeleteLocationByCarID handles the request to delete car locations by car ID
func DeleteLocationByCarID(w http.ResponseWriter, r *http.Request) {
	// Decode the JSON payload
	var requestData struct {
		CarID string `json:"car_id"`
	}
	err := json.NewDecoder(r.Body).Decode(&requestData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Extract car_id from the decoded data
	carID := requestData.CarID
	if carID == "" {
		http.Error(w, "CarID parameter is required", http.StatusBadRequest)
		return
	}

	// Create a context with a timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Get the collection from MongoDB
	collection := services.GetCollection("trackingDB", "locations")

	// Delete car locations from MongoDB
	filter := bson.M{"car_id": carID}
	_, err = collection.DeleteMany(ctx, filter)
	if err != nil {
		http.Error(w, "Failed to delete car locations from MongoDB", http.StatusInternalServerError)
		return
	}

	// Delete car locations from Redis
	err = services.RedisClient.Del(ctx, "car_location:"+carID).Err()
	if err != nil {
		http.Error(w, "Failed to delete car locations from Redis", http.StatusInternalServerError)
		return
	}

	// Return success response
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Car locations deleted successfully"))
}
