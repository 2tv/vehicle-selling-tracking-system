package main

import (
	"log"
	"net/http"

	"tracking-service/handlers"
	"tracking-service/services"

	"github.com/gorilla/mux"
)

func main() {
	// Connect to MongoDB
	services.ConnectMongoDB()

	// Connect to Redis
	services.ConnectRedis()

	r := mux.NewRouter()

	// Endpoint to get car location
	r.HandleFunc("/api/tracking", handlers.GetCarLocation).Methods("GET")

	// Endpoint to get car location
	r.HandleFunc("/api/tracking/location-history", handlers.GetCarLocationHistory).Methods("GET")

	// Endpoint to create a new car location
	r.HandleFunc("/api/tracking", handlers.CreateLocation).Methods("POST")

	r.HandleFunc("/api/tracking/location-history", handlers.DeleteLocationByCarID).Methods("DELETE")

	log.Println("Server is running on port 3003")
	log.Fatal(http.ListenAndServe(":3003", r))
}
