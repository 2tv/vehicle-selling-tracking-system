package models

import (
	"time"
)

// CarLocation represents the location data of a car
type CarLocation struct {
	CarID     string    `json:"car_id" bson:"car_id"`
	Latitude  float64   `json:"latitude" bson:"latitude"`
	Longitude float64   `json:"longitude" bson:"longitude"`
	Timestamp time.Time `json:"timestamp" bson:"timestamp"`
}

// CarInfo represents information about a car
type CarInfo struct {
	Name         string `json:"car_name"`
	Year         int    `json:"year"`
	RentingPrice string `json:"renting_price"`
	Plate        string `json:"plate"`

	// Add more fields as needed based on the response from the API
}
