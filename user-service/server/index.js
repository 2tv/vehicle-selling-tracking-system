const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const router = require("./routes");
const cors = require("cors");
const { default: mongoose } = require("mongoose");
const User = require("./models/user");

require("dotenv").config();
const port = process.env.PORT || 3001;
const mongodb_connect_string =
  process.env.MONGO_URL || "mongodb://127.0.0.1:27017/test";

// Sử dụng cors middleware ở đầu ứng dụng để gg không chặn request
app.use(cors());

// Chuyển đổi dữ liệu sang json và ngược lại
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(router);

// Khởi chạy app
mongoose
  .connect(mongodb_connect_string)
  .then(() => {
    const server = app.listen(port, () =>
      console.log(
        "> Server is up and running on port : http://localhost:" + port
      )
    );
    const io = require("socket.io")(server, {
      pingTimeout: 60000,
      cors: {
        origin: "*",
      },
    });
    io.on("connection", (socket) => {
      socket.on("setup", async (userId) => {
        try {
          userId = JSON.parse(userId);
          socket.userId = userId;
          socket.join(userId);
          const user = await User.findById(userId);
          user.isOnline = true;
          await user.save();
          socket.emit("setup", userId);
        } catch (err) {
          console.log(err);
        }
      });

      socket.on("disconnect", async () => {
        try {
          console.log("user disconnected", socket.userId);
          const user = await User.findById(socket.userId);
          user.isOnline = false;
          user.lastOnlineTime = Date.now();
          await user.save();
        } catch (err) {
          console.log(err);
        }
      });
    });
  })
  .catch((err) => console.log(err));
