const router = require("express").Router();
const authenticateJWT = require('../middleware/auth');
const express = require('express');
const cookieParser = require('cookie-parser');
const app = express();

app.use(express.json());
app.use(cookieParser());

const otpController = require("../controllers/otpController");

const {registerUser, loginUser, resetPassword, forgotPassword, getUser}  = require('../controllers/userController');

//user management and authentication
router.get('/users/:id', authenticateJWT, getUser);
router.post("/users/register", registerUser);
router.post("/users/login", loginUser);
router.post("/users/send-otp", otpController.sendOTP);
router.post("/users/forgot-password", forgotPassword);
router.post("/users/reset-password/:id/:token", resetPassword);
router.post("/users/verify", otpController.verifyOTP);

module.exports = router;
