//controllers/userController.js
const jwt = require("jsonwebtoken");
const express = require("express");
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
const User = require("../models/user");
const OTP = require("../models/otpModel"); // Sử dụng mô hình OTP hiện có
const validator = require("validator");
const app = express();
const rateLimit = require("express-rate-limit");

const limiter = rateLimit({
  windowMs: 60 * 1000, // 1 phút
  max: 5, // số lượng yêu cầu tối đa trong 1 phút
  message: "Bạn đã vượt quá số lần yêu cầu cho phép trong 1 phút",
});

// Middleware xử lý sau khi vượt quá ngưỡng
const handleRateLimitExceeded = (req, res, next) => {
  if (req.rateLimit && req.rateLimit.remaining === 0) {
    console.log("Người dùng đã vượt quá số lần yêu cầu cho phép trong 1 phút");
  }
  next();
};


app.use(bodyParser.json());

const createToken = (user) => {
  const jwtkey = process.env.JWT_SECRET_KEY;
  return jwt.sign({ id: user.id, username: user.username }, jwtkey, {
    expiresIn: "8d",
  });
};



const registerUser = async (req, res) => {
  try {
    // Retrieve the email from session
    const { username, password, email, phoneNumber, gender, dateOfBirth } =
      req.body;
    // Kiểm tra xem các trường thông tin có được cung cấp không
    if (!password || !dateOfBirth || !username || !phoneNumber || !gender)
      return res
        .status(400)
        .json({ success: false, message: "Không để trống các trường" });

    // Kiểm tra ngày sinh nhật có đủ 15 tuổi không
    const birthDate = new Date(dateOfBirth);
    const currentDate = new Date();
    const age = currentDate.getFullYear() - birthDate.getFullYear();
    if (
      age < 18 ||
      (age === 18 &&
        (currentDate.getMonth() < birthDate.getMonth() ||
          (currentDate.getMonth() === birthDate.getMonth() &&
            currentDate.getDate() < birthDate.getDate())))
    ) {
      return res.status(400).json({
        success: false,
        message: "Bạn phải ít nhất 18 tuổi mới được đăng ký...",
      });
    }
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.status(400).json({
        success: false,
        message: "Email đã tồn tại...",
      });
    }
    // Tạo một user mới và lưu vào cơ sở dữ liệu
    const user = new User({
      username,
      password,
      email,
      phoneNumber,
      gender,
      dateOfBirth,
    });
    let salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    // Tạo token và gửi lại cho client
    const token = createToken(user);
    res.status(200).json({
      _id: user._id,
      username,
      password,
      email,
      phoneNumber,
      gender,
      dateOfBirth,
      token,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: error.message });
  }
};

const verifyOTP = async (req, res) => {
  const { email, otp } = req.body;
  try {
    const otpRecord = await OTP.findOne({ email, otp });
    if (!otpRecord) {
      return res.status(400).json({ message: "Invalid OTP" });
    }

    const user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ message: "User not found" });
    }

    user.isVerified = true;
    await user.save();

    await OTP.deleteOne({ email, otp }); // Xóa OTP sau khi xác minh

    const token = createToken(user);

    return res.status(200).json({
      // _id: user._id,
      // username: user.username,
      // email: user.email,
      // phoneNumber: user.phoneNumber,
      // gender: user.gender,
      // dateOfBirth: user.dateOfBirth,
      // createdAt: user.createdAt,
      token,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server error" });
  }
};

const loginUser = [
  limiter,
  handleRateLimitExceeded,
  async (req, res) => {
    const { email, password } = req.body;
    console.log(email, password);
    try {
      // const emailRegex = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
      // if (!emailRegex.test(email)) {
      //   return res.status(400).json("Email không hợp lệ");
      // }
      let user = await User.findOne({ email });
      if (!user) return res.status(400).json("Không tìm thấy người dùng");

      console.log(password, user.password);
      const isValidPassword = await bcrypt.compare(password, user.password);
      console.log(password, user.password, isValidPassword);
      if (!isValidPassword)
        return res.status(400).json("Mật khẩu không hợp lệ");

      const token = createToken(user);
      return res.status(200).json({ _id: user._id, email, token });
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  },
];

const resetPassword = async (req, res) => {
  const id = req.params["id"];
  const token = req.params["token"];
  const { password } = req.body;
  const salt = await bcrypt.genSalt(10);

  jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
    if (err) {
      return res.json({ Status: "Error with token" });
    } else {
      bcrypt
        .hash(password, salt)
        .then((hash) => {
          User.findByIdAndUpdate({ _id: id }, { password: hash })
            .then((u) => res.send({ Status: "Success" }))
            .catch((err) => res.send({ Status: err }));
        })
        .catch((err) => res.send({ Status: err }));
    }
  });
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;

  if (!email) {
    return res.status(400).send({ Status: "Email rỗng" });
  }

  const emailRegex = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
  if (!emailRegex.test(email)) {
    return res.status(400).send({ Status: "Sai định dạng email" });
  }

  User.findOne({ email: email }).then((user) => {
    if (!user) {
      return res.send({ Status: "Người dùng không tồn tại" });
    }
    const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET_KEY, {
      expiresIn: "1d",
    });
    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS,
      },
    });
    console.log(process.env.URL2);
    var mailOptions = {
      from: process.env.MAIL_USER,
      to: email,
      subject: "Reset Password Link",
      text: process.env.URL2 + `/reset-password/${user._id}/${token}`,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        return res.send({ Status: "Success" });
      }
    });
  });
};

const getUser = async (req, res) => {
  const userId = req.params.id;
  try {
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "Không tìm thấy người dùng" });
    }
    return res.status(200).json(user);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Lỗi máy chủ" });
  }
};

const updateUserStatus = async (userId, isOnline, lastLoginAt) => {
  try {
    const user = await User.findById(userId);
    if (user) {
      user.isOnline = isOnline;
      user.lastLoginAt = lastLoginAt;
      await user.save();
    }
  } catch (error) {
    console.error("Error updating user status:", error);
  }
};

const logoutUser = async (req, res) => {
  const userId = req.user.id;
  try {
    updateUserStatus(userId, false, null);
    // Xử lý đăng xuất...
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
};

module.exports = {
  registerUser,
  verifyOTP,
  loginUser,
  forgotPassword,
  resetPassword,
  getUser,
  updateUserStatus,
  logoutUser,
};
