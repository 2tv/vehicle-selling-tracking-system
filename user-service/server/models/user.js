const Mongoose = require("mongoose");
const { Schema } = Mongoose;

const UserSchema = new Schema({
  username: String,
  password: String,
  email: String,
  phoneNumber: {
    type: String,
    default: "",
  },

  gender: String,
  dateOfBirth: {
    type: Date,
    default: Date.now,
  },

  isOnline: {
    type: Boolean,
    default: true,
  },
});

// UserSchema.methods.getDirects = () => {
//   return this.directs;
// }

module.exports = Mongoose.model("User", UserSchema, "users");
