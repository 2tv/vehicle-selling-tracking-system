const mongoose = require("mongoose");

const PaymentSchema = new mongoose.Schema({
  userId: { type: String, required: true },
  bookingId: { type: String, required: true },
  amount: { type: Number, required: true },
  method: { type: String, enum: ["cash", "momo"], required: true },
  paymentDate: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Payment", PaymentSchema);
