const axios = require("axios");
const Payment = require("../models/paymentModel");

exports.makePayment = async (req, res) => {
  const { bookingId, method } = req.body;
  const userId = req.user.id;

  try {
    const bookingResponse = await axios.get(
      `${process.env.BOOKING_SERVICE_URL}/api/bookings/${bookingId}`,
      {
        headers: {
          Authorization: req.header("Authorization"), // Thêm token vào header
        },
      }
    );
    const booking = bookingResponse.data;

    const amount = calculateAmount(booking);

    const newPayment = new Payment({ userId, bookingId, amount, method });
    await newPayment.save();

    res
      .status(201)
      .json({ message: "Payment successful", payment: newPayment });
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .json({ message: "An error occurred", error: error.message });
  }
};

// Hàm calculateAmount để tính toán số tiền dựa trên chi tiết của đơn đặt chỗ
const calculateAmount = (booking) => {
  const rentingPrice = parseFloat(booking.carDetails.renting_price); // Lấy giá thuê xe mỗi ngày từ chi tiết xe và chuyển đổi sang số thực
  const startDate = new Date(booking.bookingDate); // Lấy ngày bắt đầu thuê từ thông tin đơn đặt chỗ và chuyển đổi sang kiểu Date
  const currentDate = new Date(); // Lấy ngày hiện tại (hoặc ngày trả xe) và chuyển đổi sang kiểu Date

  // Tính toán số ngày thuê xe
  const timeDiff = Math.abs(currentDate - startDate); // Tính chênh lệch thời gian giữa ngày hiện tại và ngày bắt đầu thuê
  const daysRented = Math.ceil(timeDiff / (1000 * 60 * 60 * 24)); // Chuyển đổi chênh lệch thời gian sang số ngày thuê (1 ngày = 1000 ms * 60 s * 60 phút * 24 giờ)

  // Tính tổng số tiền phải trả
  const totalAmount = rentingPrice * daysRented; // Tổng số tiền = Giá thuê mỗi ngày * Số ngày thuê

  return totalAmount; // Trả về tổng số tiền
};
