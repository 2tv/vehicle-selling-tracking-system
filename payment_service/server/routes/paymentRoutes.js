const express = require("express");
const { makePayment } = require("../controllers/paymentController");
const authenticateJWT = require("../middleware/auth");

const router = express.Router();

router.post("/api/pay", authenticateJWT, makePayment);

module.exports = router;
