from django.db import models

# Create your models here.

class Car(models.Model):
    car_name = models.CharField(max_length=100)
    year = models.IntegerField()
    renting_price = models.DecimalField(max_digits=10, decimal_places=2)
    plate = models.CharField(max_length=20)
    kms_driven = models.IntegerField()
    fuel_type = models.CharField(max_length=50)
    transmission = models.CharField(max_length=50)
    is_booking = models.BooleanField(default=False)

    def __str__(self):
        return self.car_name
