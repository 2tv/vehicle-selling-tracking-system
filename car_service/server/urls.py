from django.urls import path
from .views import CarListView
from . import views

urlpatterns = [
    path('cars/', CarListView.as_view(), name='car_list'),
    path('cars/<str:pk>/', views.car_detail, name='car_detail'),
    path('cars/delete/<str:pk>/', views.car_delete, name='views.car_delete'),
    # Các URL khác ở đây...
]
