from django.http import  JsonResponse
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from dotenv import load_dotenv

# Load biến môi trường từ tệp .env
load_dotenv()
import jwt, os
from .models import Car
from .serializers import CarSerializer
# Create your views here.

secret_key = os.getenv("JWT_SECRET_KEY")

def authenticate(jwt_token, secret_key):
    try:
        decoded_token = jwt.decode(jwt_token, secret_key, algorithms=["HS256"])
        # Giải mã thành công, decoded_token là một dictionary chứa thông tin từ JWT
        # Bạn có thể làm bất kỳ xử lý nào dựa trên decoded_token ở đây
    except jwt.ExpiredSignatureError:
        # Xử lý khi JWT hết hạn
        return False
    except jwt.InvalidTokenError:
        # Xử lý khi JWT không hợp lệ
        return False
    if(jwt_token is None):
        return False
    return True

class CarListView(generics.GenericAPIView):
    authentication_classes = []  # Bỏ qua xác thực
    permission_classes = [AllowAny]  # Cho phép mọi người truy cập
    def get_queryset(self):
        return Car.objects.filter(is_booking=False)


    def get(self, *args, **kwargs):
        # Lấy giá trị của tiêu đề 'X-Your-Header' từ yêu cầu
        jwt_token = self.request.META.get('HTTP_AUTHORIZATION')

        if(not authenticate(jwt_token, secret_key)):
            return Response({"error":"Not Login"},status=403)

        paginator = PageNumberPagination()
        cars = self.get_queryset()
        result_page = paginator.paginate_queryset(cars, self.request)
        serializer = CarSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    def post(self, request, *args, **kwargs):
        jwt_token = self.request.META.get('HTTP_AUTHORIZATION')
        if(not authenticate(jwt_token, secret_key)):
            return Response({"error":"Not Login"},status=403)
        serializer = CarSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def car_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    jwt_token = request.META.get('HTTP_AUTHORIZATION')
    if(not authenticate(jwt_token, secret_key)):
        return JsonResponse({"error":"Not Login"},status=403)
    try:
        car = Car.objects.get(pk=pk)
    except Car.DoesNotExist:
        return JsonResponse({"error": "Car not exist"}, status=404)


    if request.method == 'GET':
        serializer = CarSerializer(car)
        return JsonResponse(serializer.data,status=200)

def car_delete(request, pk, format=None):
    jwt_token = request.META.get('HTTP_AUTHORIZATION')
    if(not authenticate(jwt_token, secret_key)):
        return JsonResponse({"error":"Not Login"},status=403)
    try:
        car = Car.objects.get(pk=pk)
    except Car.DoesNotExist:
        return JsonResponse({"error": "Car not exist"}, status=404)

    car.delete()
    return JsonResponse({"message": "Delete success"},status=200)
