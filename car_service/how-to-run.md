## 1. Install Python

## Change dir to project
`` $ cd car_service``

## 2. Active virtual enviroment
If you doesn't have venv in your workspace then:

    $ py -m venv venv
    $ venv\Scripts\activate

## 3. Install package

    $ pip install -r requirements.txt

## 4. First time run app

### 4.1. Create database

Create database name 'Car_Service'

#### Go to file 'car_service\settings.py', find this

![alt text](https://i.imgur.com/gHjJzHD.png)

Change User and password


### 4.2. Migrate

``py manage.py migrate``

### Note: If code have some change in project
    py manage.py makemigrations
    py manage.py migrate

## 5. Run app
    py manage.py runserver
