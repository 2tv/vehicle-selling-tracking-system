package iuh.spring.gateway;

import java.nio.charset.StandardCharsets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.http.server.reactive.ServerHttpResponse;
import reactor.core.publisher.Mono;
import org.springframework.http.HttpHeaders;
@SpringBootApplication
@EnableAutoConfiguration
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
	}
    @Bean
    public KeyResolver apiKeyResolver() {
        return exchange -> {
            HttpHeaders headers = exchange.getRequest().getHeaders();
            String authorizationHeader = headers.getFirst("Authorization");
            if (authorizationHeader != null && !authorizationHeader.isEmpty()) {
                return Mono.just(authorizationHeader);
            } else {
                return Mono.just("1"); // Thay đổi "default_value" thành giá trị mặc định mong muốn của bạn
            }
        };
    }

    // @Bean
    // public CustomRateLimitErrorHandler customRateLimitErrorHandler() {
    //     return new CustomRateLimitErrorHandler();
    // }

    // @Component
    // @Order(-2)
    // public static class CustomRateLimitErrorHandler implements ErrorWebExceptionHandler {

    //     @Override
    //     public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
    //         if (ex instanceof org.springframework.cloud.gateway.support.NotFoundException) {
    //             ServerHttpResponse response = exchange.getResponse();
    //             response.setStatusCode(HttpStatus.TOO_MANY_REQUESTS);
    //             response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
    //             String errorMessage = "{\"message\": \"Số lượng yêu cầu đã vượt quá giới hạn\"}";
    //             byte[] bytes = errorMessage.getBytes(StandardCharsets.UTF_8);
    //             return response.writeWith(Mono.just(response.bufferFactory().wrap(bytes)));
    //         }
    //         return Mono.error(ex);
    //     }
    // }
}


